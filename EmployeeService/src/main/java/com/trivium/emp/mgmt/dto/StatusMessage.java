package com.trivium.emp.mgmt.dto;

import java.io.Serializable;

public class StatusMessage implements Serializable {
	private static final long serialVersionUID = 1L;

	private int statusId;
	private String statusMessage;

	public StatusMessage() {
		super();
	}

	public StatusMessage(int statusId, String statusMessage) {
		super();
		this.statusId = statusId;
		this.statusMessage = statusMessage;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

}
