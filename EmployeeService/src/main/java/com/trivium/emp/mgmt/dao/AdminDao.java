package com.trivium.emp.mgmt.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import com.trivium.emp.mgmt.entity.Admin;
import com.trivium.emp.mgmt.util.HibernateUtil;

/**
 * @author Sai.Krishna
 *
 */
@Repository
public class AdminDao {
	public static final Logger logger = LogManager.getLogger(AdminDao.class);

	public boolean addAdmin(Admin admin) {
		boolean isSuccess = false;
		Transaction transaction = null;
		try (Session session = HibernateUtil.openSession()) {
			transaction = session.getTransaction();
			transaction.begin();
			session.save(admin);
			isSuccess = true;
			transaction.commit();
		} catch (Exception exception) {
			if (transaction != null)
				transaction.rollback();

			logger.error(exception.toString());
		}

		return isSuccess;
	}

	public boolean validateUser(Admin admin) {
		boolean isValid = false;

		try (Session session = HibernateUtil.openSession();) {
			CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
			CriteriaQuery<Admin> criteriaQuery = criteriaBuilder.createQuery(Admin.class);
			Root<Admin> root = criteriaQuery.from(Admin.class);

			criteriaQuery.select(root);
			criteriaQuery.where(criteriaBuilder.equal(root.get("userName"), admin.getUserName()),
					criteriaBuilder.equal(root.get("password"), admin.getPassword()));
			Query<Admin> query = session.createQuery(criteriaQuery);
			List<Admin> adList = query.getResultList();

			if (!adList.isEmpty())
				isValid = true;
		}

		catch (Exception exception) {
			logger.error(exception.toString());
		}

		return isValid;
	}
}
