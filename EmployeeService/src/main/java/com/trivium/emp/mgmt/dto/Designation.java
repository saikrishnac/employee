package com.trivium.emp.mgmt.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class Designation implements Serializable {
	private static final long serialVersionUID = 1L;

	private String designation;
	private BigInteger number;

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public BigInteger getNumber() {
		return number;
	}

	public void setNumber(BigInteger number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return "Designation [designation=" + designation + ", number=" + number + "]";
	}

}
