package com.trivium.emp.mgmt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.trivium.emp.mgmt.dao.EmployeeDao;
import com.trivium.emp.mgmt.dto.Designation;
import com.trivium.emp.mgmt.dto.StatusMessage;
import com.trivium.emp.mgmt.entity.Employee;

@Controller
public class EmployeeController {
	@Autowired(required = true)
	EmployeeDao employeeDao;

	String errorMessage = "Something went wrong try again later";

	@PostMapping(path = "/addEmployee", produces = "application/json")
	public @ResponseBody StatusMessage addEmployee(@RequestBody Employee employee) {
		StatusMessage message;

		boolean status = employeeDao.addEmployee(employee);

		if (status)
			message = new StatusMessage(200, "Employee Added successfuly");
		else
			message = new StatusMessage(500, errorMessage);

		return message;
	}

	@PostMapping(path = "/updateEmployee", produces = "application/json")
	public @ResponseBody StatusMessage updateEmployee(@RequestBody Employee employee) {
		StatusMessage message;

		boolean status = employeeDao.updateEmployee(employee);

		if (status)
			message = new StatusMessage(200, "Employee Updated successfuly");
		else
			message = new StatusMessage(500, errorMessage);

		return message;
	}

	@DeleteMapping("/deleteEmployee/{id}")
	public @ResponseBody StatusMessage deleteEmployee(@PathVariable int id) {
		StatusMessage message;

		boolean status = employeeDao.deleteEmployee(id);

		if (status)
			message = new StatusMessage(200, "Employee Deleted successfuly");
		else
			message = new StatusMessage(500, errorMessage);

		return message;
	}

	@GetMapping(path = "/getEmployeeDetails", produces = "application/json")
	public @ResponseBody List<Employee> getEmployees() {

		List<Employee> employees = null;
		employees = employeeDao.getAllEmployee();

		return employees;
	}

	@GetMapping(path = "/getDeletedEmployeeDetails", produces = "application/json")
	public @ResponseBody List<Employee> getDeletedEmployees() {

		List<Employee> employees = null;
		employees = employeeDao.getAllDeletedEmployee();

		return employees;
	}

	@GetMapping("/getEmployee/{id}")
	public @ResponseBody Employee getEmployeeById(@PathVariable int id) {
		Employee employee = null;
		employee = employeeDao.getEmployeeById(id);

		return employee;
	}

	@GetMapping("/getEmployeeDesignation")
	public @ResponseBody List<Designation> getEmployeeDesignationCount() {
		List<Designation> designationList = null;
		designationList = employeeDao.getEmployeeCountWithDesignation();

		return designationList;
	}
}
