package com.trivium.emp.mgmt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.trivium.emp.mgmt.dao.AdminDao;
import com.trivium.emp.mgmt.dto.StatusMessage;
import com.trivium.emp.mgmt.entity.Admin;

/**
 * @author Sai.Krishna
 *
 */
@Controller
public class LoginController 
{
	@Autowired (required=true)
	AdminDao adminDao;
	
	@PostMapping (path ="/login",  produces = "application/json")
	public @ResponseBody StatusMessage login (@RequestBody Admin admin)
	{
		StatusMessage message;
		
		boolean status = adminDao.validateUser (admin);
		
		if (status)
			message = new StatusMessage(200, "logged in Successfuly");
		else
			message = new StatusMessage(404, "unauthorised");
			
		return message;
	}
}
