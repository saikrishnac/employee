package com.trivium.emp.mgmt.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.trivium.emp.mgmt.dto.Designation;
import com.trivium.emp.mgmt.entity.Employee;
import com.trivium.emp.mgmt.util.HibernateUtil;

/**
 * @author Sai.Krishna
 *
 */
@Repository
public class EmployeeDao {

	public static final Logger logger = LogManager.getLogger(EmployeeDao.class);

	public EmployeeDao() {
		super();
	}

	public boolean addEmployee(Employee employee) {
		boolean isSuccess = false;
		Transaction transaction = null;

		try (Session session = HibernateUtil.openSession()) {

			transaction = session.getTransaction();
			transaction.begin();
			session.save(employee);
			isSuccess = true;
			transaction.commit();
		} catch (Exception exception) {
			if (transaction != null)
				transaction.rollback();

			logger.error(exception.toString());
		}

		return isSuccess;
	}

	public Employee getEmployeeById(int employeeId) {
		Employee employee = new Employee();

		try (Session session = HibernateUtil.openSession()) {
			employee = session.get(Employee.class, employeeId);
		} catch (Exception exception) {
			logger.error(exception.toString());
		}
		return employee;
	}

	public boolean updateEmployee(Employee employee) {
		boolean isSuccess = false;
		Transaction transaction = null;

		try (Session session = HibernateUtil.openSession()) {

			transaction = session.getTransaction();
			transaction.begin();
			session.update(employee);
			isSuccess = true;
			transaction.commit();
		} catch (Exception exception) {
			if (transaction != null)
				transaction.rollback();

			logger.error(exception.toString());
		}

		return isSuccess;
	}

	public boolean deleteEmployee(int employeeId) {
		boolean isSuccess = false;
		Transaction transaction = null;

		try (Session session = HibernateUtil.openSession()) {

			transaction = session.getTransaction();
			transaction.begin();
			Employee employee = session.get(Employee.class, employeeId);
			employee.setIsDeleted(1);
			session.update(employee);
			isSuccess = true;
			transaction.commit();
		} catch (Exception exception) {
			if (transaction != null)
				transaction.rollback();

			logger.error(exception.toString());
		}

		return isSuccess;
	}

	public List<Employee> getAllEmployee() {

		List<Employee> empList = new ArrayList<>();

		try (Session session = HibernateUtil.openSession();) {

			CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
			CriteriaQuery<Employee> criteriaQuery = criteriaBuilder.createQuery(Employee.class);
			Root<Employee> root = criteriaQuery.from(Employee.class);

			criteriaQuery.select(root);
			criteriaQuery.where(criteriaBuilder.notEqual(root.get("isDeleted"), "1"));
			criteriaQuery.orderBy(criteriaBuilder.asc(root.get("employeeId")));
			Query<Employee> query = session.createQuery(criteriaQuery);
			empList = query.getResultList();
		}

		catch (Exception exception) {
			logger.error(exception.toString());
		}

		return empList;
	}
	
	public List<Employee> getAllDeletedEmployee () {

		List<Employee> empList = new ArrayList<>();

		try (Session session = HibernateUtil.openSession();) {

			CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
			CriteriaQuery<Employee> criteriaQuery = criteriaBuilder.createQuery(Employee.class);
			Root<Employee> root = criteriaQuery.from(Employee.class);

			criteriaQuery.select(root);
			criteriaQuery.where(criteriaBuilder.equal(root.get("isDeleted"), "1"));
			criteriaQuery.orderBy(criteriaBuilder.asc(root.get("employeeId")));
			Query<Employee> query = session.createQuery(criteriaQuery);
			empList = query.getResultList();
		}

		catch (Exception exception) {
			logger.error(exception.toString());
		}

		return empList;
	}

	public List<Designation> getEmployeeCountWithDesignation() {
		List<Designation> designationList = null;
		try (Session session = HibernateUtil.openSession()) {

			session.beginTransaction();

			@SuppressWarnings({ "deprecation", "unchecked" })
			List<Designation> list = session.createNativeQuery(
					"select employee.designation, count(employee.designation) as number From Employee employee where employee.is_deleted = 0  group By employee.designation")
					.setResultTransformer(Transformers.aliasToBean(Designation.class)).list();
			designationList = list;

		}

		catch (Exception exception) {
			logger.error(exception.toString());
		}

		return designationList;
	}

}
