package com.trivium.emp.mgmt.dao;

import com.trivium.emp.mgmt.entity.Admin;

public class EmployeeDaoTest 
{
	public static void main(String[] args)
	{
		AdminDao adminDao = new AdminDao();
		
		Admin admin = new Admin();
		admin.setUserName("admin1");
		admin.setPassword("admin");
		
		System.out.println(adminDao.validateUser (admin));
	}
}
