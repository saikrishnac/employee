package com.trivium.emp.mgmt.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@ManagedBean
@ViewScoped
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;

	private int employeeId;
	private String firstName;
	private String lastName;
	private String mobile;
	private String location;
	private Date dateOfBirth;
	private String designation;
	private String gender;
	private Date createddate;
	private int isDeleted;

	private Map<String, String> genders;
	private Map<String, String> designations;

	public Employee() {
		super();

		genders = new HashMap<>();
		genders.put("Male", "Male");
		genders.put("Female", "Female");
		genders.put("Other", "Other");

		designations = new HashMap<>();
		designations.put("Trainee Engineer", "Trainee Engineer");
		designations.put("Associate Software Engineer", "Associate Software Engineer");
		designations.put("Software Engineer", "Software Engineer");
		designations.put("Senior Software Engineer", "Senior Software Engineer");

		designations.put("Trainee Test Engineer", "Trainee Test Engineer");
		designations.put("Associate Test Engineer", "Associate Test Engineer");
		designations.put("Test Engineer", "Test Engineer");
		designations.put("Senior Test Engineer", "Senior Test Engineer");
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", mobile=" + mobile + ", location=" + location + ", dateOfBirth=" + dateOfBirth + ", designation="
				+ designation + ", gender=" + gender + ", createddate=" + createddate + ", isDeleted=" + isDeleted
				+ "]";
	}

	public Map<String, String> getGenders() {
		return genders;
	}

	public void setGenders(Map<String, String> genders) {
		this.genders = genders;
	}

	public Map<String, String> getDesignations() {
		return designations;
	}

	public void setDesignations(Map<String, String> designations) {
		this.designations = designations;
	}
}
