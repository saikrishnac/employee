/**
 * 
 */
package com.trivium.emp.mgmt.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * @author Sai.Krishna
 *
 */
@ManagedBean
@ViewScoped
public class DropDownView implements Serializable {
	private static final long serialVersionUID = 1L;

	private Map<String, String> designations;
	private Map<String, String> genders;

	private String gender;

	public DropDownView() {
		super();
		designations = new HashMap<>();
		designations.put("Trainee Engineer", "Trainee Engineer");
		designations.put("Associate Software Engineer", "Associate Software Engineer");
		designations.put("Software Engineer", "Software Engineer");
		designations.put("Senior Software Engineer", "Senior Software Engineer");

		designations.put("Trainee Test Engineer", "Trainee Test Engineer");
		designations.put("Associate Test Engineer", "Associate Test Engineer");
		designations.put("Test Engineer", "Test Engineer");
		designations.put("Senior Test Engineer", "Senior Test Engineer");

		genders = new HashMap<>();
		genders.put("Male", "Male");
		genders.put("Female", "Female");
		genders.put("Other", "Other");

	}

	public Map<String, String> getDesignations() {
		return designations;
	}

	public void setDesignations(Map<String, String> designations) {
		this.designations = designations;
	}

	public Map<String, String> getGenders() {
		return genders;
	}

	public void setGenders(Map<String, String> genders) {
		this.genders = genders;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

}
