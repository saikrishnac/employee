/**
 * 
 */
package com.trivium.emp.mgmt.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.PieChartModel;

import com.trivium.emp.mgmt.entity.Designation;

/**
 * @author Sai.Krishna
 *
 */

@ManagedBean
@ViewScoped
public class ChartView implements Serializable {
	private static final long serialVersionUID = 1L;

	private PieChartModel pieModel;
	private BarChartModel barModel;

	public ChartView() {
		super();
		createPieModel();
	}

	public PieChartModel getPieModel() {
		return pieModel;
	}

	public void setPieModel(PieChartModel pieModel) {
		this.pieModel = pieModel;
	}

	public BarChartModel getBarModel() {
		return barModel;
	}

	public void setBarModel(BarChartModel barModel) {
		this.barModel = barModel;
	}

	private void createPieModel() {
		pieModel = new PieChartModel();

		DesignationBean designationBean = new DesignationBean();
		List<Designation> designations = designationBean.getDesignationList();

		for (Designation designation : designations) {
			pieModel.set(designation.getDesignation() + "(" + designation.getNumber() + ")", designation.getNumber());
		}

		pieModel.setTitle("Number Of Employees");
		pieModel.setLegendPosition("w");
		pieModel.setShadow(true);
	}
}
