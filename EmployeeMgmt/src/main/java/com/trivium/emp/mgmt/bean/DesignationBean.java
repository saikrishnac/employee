/**
 * 
 */
package com.trivium.emp.mgmt.bean;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.trivium.emp.mgmt.entity.Designation;

/**
 * @author Sai.Krishna
 *
 */
public class DesignationBean {
	public static final Logger logger = LogManager.getLogger(DesignationBean.class);

	public List<Designation> getDesignationList() {
		List<Designation> designationList = null;

		try {
			RestTemplate restTemplate = new RestTemplate();

			ResponseEntity<List<Designation>> response = restTemplate.exchange(
					"http://localhost:8080/EmployeeService/getEmployeeDesignation", HttpMethod.GET, null,
					new ParameterizedTypeReference<List<Designation>>() {
					});

			designationList = response.getBody();
		} catch (RestClientException restClientException) {
			logger.error(restClientException.toString());
		}

		return designationList;
	}
}
