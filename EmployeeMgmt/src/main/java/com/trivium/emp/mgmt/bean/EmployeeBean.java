package com.trivium.emp.mgmt.bean;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.trivium.emp.mgmt.entity.Designation;
import com.trivium.emp.mgmt.entity.Employee;
import com.trivium.emp.mgmt.entity.StatusMessage;

public class EmployeeBean {

	public static final Logger logger = LogManager.getLogger(EmployeeBean.class);

	public List<Employee> getEmployeeList() {
		List<Employee> empList = null;

		try {
			RestTemplate restTemplate = new RestTemplate();

			ResponseEntity<List<Employee>> response = restTemplate.exchange(
					"http://localhost:8080/EmployeeService/getEmployeeDetails", HttpMethod.GET, null,
					new ParameterizedTypeReference<List<Employee>>() {
					});

			empList = response.getBody();
		} catch (RestClientException restClientException) {
			logger.error(restClientException.toString());
		}

		return empList;
	}

	public List<Employee> getDeletedEmployeeList() {
		List<Employee> empList = null;

		try {
			RestTemplate restTemplate = new RestTemplate();

			ResponseEntity<List<Employee>> response = restTemplate.exchange(
					"http://localhost:8080/EmployeeService/getDeletedEmployeeDetails", HttpMethod.GET, null,
					new ParameterizedTypeReference<List<Employee>>() {
					});

			empList = response.getBody();
		} catch (RestClientException restClientException) {
			logger.error(restClientException.toString());
		}

		return empList;
	}

	public Employee getEmployeeInfo(int employeeId) {
		Employee employee = null;

		try {
			RestTemplate restTemplate = new RestTemplate();

			employee = restTemplate.getForObject("http://localhost:8080/EmployeeService/getEmployee/" + employeeId,
					Employee.class);
		} catch (RestClientException restClientException) {
			logger.error(restClientException.toString());
		}
		return employee;
	}

	public List<Designation> getDesignationList() {
		List<Designation> desList = null;

		try {
			RestTemplate restTemplate = new RestTemplate();

			ResponseEntity<List<Designation>> response = restTemplate.exchange(
					"http://localhost:8080/EmployeeService/getEmployeeDesignation", HttpMethod.GET, null,
					new ParameterizedTypeReference<List<Designation>>() {
					});

			desList = response.getBody();
			for (Designation designation : desList) {
				System.out.println(designation);
			}

		} catch (RestClientException restClientException) {
			logger.error(restClientException.toString());
		}
		return desList;
	}

	public boolean addEmployee(Employee employee) {
		boolean isValid = false;
		try {
			RestTemplate restTemplate = new RestTemplate();

			ResponseEntity<StatusMessage> message = restTemplate
					.postForEntity("http://localhost:8080/EmployeeService/addEmployee", employee, StatusMessage.class);

			StatusMessage statusMessage = message.getBody();

			if (statusMessage.getStatusId() == 200)
				isValid = true;

		} catch (RestClientException restClientException) {
			logger.error(restClientException.toString());
		}

		return isValid;
	}

	public boolean updateEmployee(Employee employee) {
		boolean isValid = false;
		try {
			RestTemplate restTemplate = new RestTemplate();

			ResponseEntity<StatusMessage> message = restTemplate.postForEntity(
					"http://localhost:8080/EmployeeService/updateEmployee", employee, StatusMessage.class);

			StatusMessage statusMessage = message.getBody();

			if (statusMessage.getStatusId() == 200)
				isValid = true;

		} catch (RestClientException restClientException) {
			logger.error(restClientException.toString());
		}

		return isValid;
	}

	/**
	 * @param employeeId
	 * @return
	 */
	public boolean deleteEmployee(int employeeId) {
		boolean isValid = true;
		try {
			RestTemplate restTemplate = new RestTemplate();

			restTemplate.delete("http://localhost:8080/EmployeeService/deleteEmployee/" + employeeId);

		} catch (RestClientException restClientException) {
			isValid = false;
			logger.error(restClientException.toString());
		}

		return isValid;
	}

}
