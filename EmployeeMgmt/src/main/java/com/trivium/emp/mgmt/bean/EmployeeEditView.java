/**
 * 
 */
package com.trivium.emp.mgmt.bean;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;

import com.trivium.emp.mgmt.entity.Employee;

/**
 * @author Sai.Krishna
 *
 */

@ManagedBean(name = "dtEditView")
@ViewScoped
public class EmployeeEditView implements Serializable {

	private static final long serialVersionUID = 1L;

	public void onRowEdit(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("Employee Edited", ((Employee) event.getObject()).getFirstName());
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void onRowCancel(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("Edit Cancelled", ((Employee) event.getObject()).getFirstName());
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void onCellEdit(CellEditEvent event) {
		Object newValue = event.getNewValue();

		Employee employee = (Employee) newValue;
		System.out.println(employee);
	}
}
