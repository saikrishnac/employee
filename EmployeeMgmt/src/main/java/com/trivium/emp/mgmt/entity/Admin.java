package com.trivium.emp.mgmt.entity;

import java.io.Serializable;

/**
 * @author Sai.Krishna
 *
 */
public class Admin implements Serializable {
	private static final long serialVersionUID = 1L;

	private int adminId;
	private String userName;
	private String password;

	public Admin() {
		super();
	}

	public int getAdminId() {
		return adminId;
	}

	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Admin [adminId=" + adminId + ", userName=" + userName + ", password=" + password + "]";
	}
}
