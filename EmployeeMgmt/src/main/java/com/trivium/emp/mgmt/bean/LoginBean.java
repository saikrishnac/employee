package com.trivium.emp.mgmt.bean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.trivium.emp.mgmt.entity.Admin;
import com.trivium.emp.mgmt.entity.StatusMessage;

/**
 * @author Sai.Krishna
 *
 */
public class LoginBean {
	public static final Logger logger = LogManager.getLogger(LoginBean.class);

	public boolean validateAdmin(Admin admin) {
		boolean isValid = false;
		try {
			RestTemplate restTemplate = new RestTemplate();

			ResponseEntity<StatusMessage> message = restTemplate
					.postForEntity("http://localhost:8080/EmployeeService/login", admin, StatusMessage.class);

			StatusMessage statusMessage = message.getBody();

			if (statusMessage.getStatusId() == 200)
				isValid = true;

		} catch (RestClientException restClientException) {
			logger.error(restClientException.toString());
		}

		return isValid;
	}
}
