package com.trivium.emp.mgmt.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.event.RowEditEvent;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.trivium.emp.mgmt.entity.Admin;
import com.trivium.emp.mgmt.entity.Employee;

/**
 * @author Sai.Krishna
 *
 */
@ManagedBean
@ViewScoped
public class UserLoginView implements Serializable {
	public static final Logger logger = LogManager.getLogger(UserLoginView.class);

	private static final long serialVersionUID = 1L;
	private String login = "Welcome";
	private String username;

	private List<Employee> employeeList;

	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	public List<Employee> getEmployeeList() {
		try {
			EmployeeBean employeeBean = new EmployeeBean();
			if (employeeList == null)
				employeeList = employeeBean.getEmployeeList();
		} catch (RestClientException restClientException) {
			logger.error(restClientException.toString());
		}

		return employeeList;
	}

	public void login() throws IOException {
		FacesMessage message = null;
		Admin admin = new Admin();
		admin.setUserName(username);
		admin.setPassword(password);

		LoginBean loginBean = new LoginBean();

		if (loginBean.validateAdmin(admin)) {
			message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Welcome", username);
			FacesContext.getCurrentInstance().getExternalContext().redirect("faces/pie.xhtml");
		} else {
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Loggin Error", "Invalid credentials");
		}

		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public void addEmployee(Employee employee) throws IOException {
		FacesMessage message = null;

		EmployeeBean employeeBean = new EmployeeBean();

		boolean status = employeeBean.addEmployee(employee);

		if (status) {
			employeeList = null;
			message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Newly Added Employee", employee.getFirstName());
		} else {
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", " Try after Some time");
		}

		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public List<Employee> getDeletedEmployeeList() {
		List<Employee> empList = null;

		try {
			EmployeeBean employeeBean = new EmployeeBean();
			empList = employeeBean.getDeletedEmployeeList();
		} catch (RestClientException restClientException) {
			logger.error(restClientException.toString());
		}

		return empList;
	}

	public void onRowEdit(RowEditEvent event) {
		FacesMessage msg = null;
		Employee employee = (Employee) event.getObject();
		EmployeeBean employeeBean = new EmployeeBean();

		boolean status = employeeBean.updateEmployee(employee);

		if (status)
			msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Employee Edited", "" + employee.getEmployeeId());
		else
			msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Something went wrong please try again");

		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void onRowCancel(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("Edit Cancelled", ((Employee) event.getObject()).getFirstName());
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void delete(Employee employee) {
		FacesMessage msg = null;
		EmployeeBean employeeBean = new EmployeeBean();
		logger.warn(employee);
		boolean status = employeeBean.deleteEmployee(employee.getEmployeeId());

		if (status) {
			employeeList = null;
			msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Employee Deleted", employee.getFirstName());
		} else
			msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Something went wrong please try again");

		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
}